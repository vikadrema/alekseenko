function task1() {
    let value = prompt("Введіть трицифрове число:");
    alert("Звороній порядок числа " + value + " буде " + reverseString(value));
}

function reverseString(str) {
    let splitString = str.split("");
    let reverseArray = splitString.reverse();
    return reverseArray.join("");
}

function task2() {
    let vectorA = prompt("Введіть вектор А у форматі x,y,z:").split(",");
    let vectorB = prompt("Введіть вектор B у форматі x,y,z:").split(",");
    let vectorC = prompt("Введіть вектор C у форматі x,y,z:").split(",");

    let isComplanar = checkIsComplanar(vectorA, vectorB, vectorC);

    let result = "Вектори ";
    if (!isComplanar) {
        result += "не ";
    }
    result += "є компланарні";
    alert(result);
}

function checkIsComplanar(vectorA, vectorB, vectorC) {
    let valueA = vectorA[0] * vectorB[1] * vectorC[2] + vectorA[1] * vectorB[2] * vectorC[0] + vectorA[2] * vectorB[0] * vectorC[1];
    let valueB = -(vectorA[2] * vectorB[1] * vectorC[0]) - (vectorA[1] * vectorB[0] * vectorC[2]) - (vectorA[0] * vectorB[2] * vectorC[1]);
    let res = valueA + valueB;
    return res === 0;
}

function task3() {
    let value = prompt("Введіть число:");
    alert("Кількість чотиризначних чисел із даною сумою: " + countSumValues(value));
}

function countSumValues(value) {
    let count = 0;

    for (let a = 0; a < 10; a++) {
        for (let b = 0; b < 10; b++) {
            for (let c = 0; c < 10; c++) {
                for (let d = 0; d < 10; d++) {
                    let sum = a + b + c + d;
                    if (sum === value)
                        count++;
                }
            }
        }
    }

    return count;
}

function task4() {
    let number = prompt("Введіть число");
    let currentSimpleNumber = 2;
    let indexOfSimple = 0;
    const simpleNumbers = [];
    let result = number + " = ";
    while (currentSimpleNumber <= number) {
        while (number % currentSimpleNumber === 0) {
            number = number / currentSimpleNumber;
            simpleNumbers[indexOfSimple] = currentSimpleNumber;
            indexOfSimple++;
            if (number === 1) {
                break;
            }
        }
        currentSimpleNumber++;
    }
    for (let i = 0; i < simpleNumbers.length; i++) {
        result += simpleNumbers[i];

        if (i !== simpleNumbers.length - 1) {
            result += " * ";
        }
    }

    alert(result);
}