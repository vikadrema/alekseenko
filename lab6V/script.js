function task1() {
    let count = +prompt("Введіть розмір масиву:");
    let arr = [];

    for (let i = 0; i < count; i++) {
        let number = +prompt("Введіть " + i + " елемент масиву:");
        arr.push(number);
    }

    let sum = 0;
    let plusCount = 5;
    for (let i = count - 1; i >= 0; i--) {
        if (arr[i] > 0) {
            if (plusCount !== 0) {
                sum += arr[i];
                plusCount--;
            } else
                break;
        }
    }

    alert("Сумма 5-ти останніх додатніх чисел = " + sum)
}

function task2() {
    let value = +prompt("Введіть число n для пошуку його факторіалу:");
    alert(factorial(value));
}

function factorial(n) {
    return (n !== 1) ? n * factorial(n - 1) : 1;
}

function task3() {
    const arr = [1, 2, 3];
    alert("Аргумент [" + arr + "] , розмір масиву = " + arrayLength(arr));
    alert("Аргумент пустий, тому " + arrayLength());
}

function arrayLength(array) {
    if(array == null)
        return "Error";

    return array.length;
}